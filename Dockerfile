# syntax=docker/dockerfile:1.1.7-experimental
#############################################
# Tox testsuite for multiple python version #
#############################################
FROM advian/tox-base:alpine-3.16 as tox
ARG PYTHON_VERSIONS="3.10 3.9 3.8 3.7 3.6"
ARG POETRY_VERSION="1.2.2"
RUN export RESOLVED_VERSIONS=`pyenv_resolve $PYTHON_VERSIONS` \
    && echo RESOLVED_VERSIONS=$RESOLVED_VERSIONS \
    && for pyver in $RESOLVED_VERSIONS; do pyenv install -s $pyver; done \
    && pyenv global $RESOLVED_VERSIONS \
    && poetry self update $POETRY_VERSION || pip install -U poetry==$POETRY_VERSION \
    && pip install -U tox \
    && apk add --no-cache \
        zeromq-dev \
        "gtk+3.0-dev" \
        py3-cairo \
        py3-cairo-dev \
        py3-cairocffi \
        cairo-dev \
        py3-gobject3-dev \
        gobject-introspection-dev \
        xvfb-run \
        git \
    && true

######################
# Base builder image #
######################
FROM alpine:3.16 as builder_base

ENV \
  # locale
  LC_ALL=C.UTF-8 \
  # python:
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # poetry:
  POETRY_VERSION=1.2.2

RUN apk add --no-cache \
        curl \
        git \
        bash \
        tini \
        build-base \
        libffi-dev \
        linux-headers \
        openssl \
        openssl-dev \
        zeromq \
        zeromq-dev \
        python3 \
        python3-dev \
        py3-virtualenv \
        py3-pip \
        py3-gobject3 \
        py3-gobject3-dev \
        "gtk+3.0" \
        "gtk+3.0-dev" \
        py3-cairo \
        py3-cairo-dev \
        py3-cairocffi \
        cairo-dev \
        gobject-introspection-dev \
        font-noto \
        gnome-icon-theme \
        adwaita-icon-theme \
        tini \
        musl-dev \
        "g++" \
        libgcc \
        openssh-client \
        cargo \
    # Set default python3 and pip3 to 3.10
    && ln -f -s /usr/bin/python3.10 /usr/local/bin/python3 \
    && ln -f -s /usr/bin/python3.10 /usr/local/bin/python \
    && ln -f -s /usr/bin/pip3 /usr/local/bin/pip3 \
    && ln -f -s /usr/bin/pip3 /usr/local/bin/pip \
    # githublab ssh
    && mkdir -p -m 0700 ~/.ssh && ssh-keyscan gitlab.com github.com | sort > ~/.ssh/known_hosts \
    # Installing `poetry` package manager:
    && curl -sSL https://install.python-poetry.org | python3 - \
    && echo 'export PATH="/root/.local/bin:$PATH"' >>/root/.profile \
    && export PATH="/root/.local/bin:$PATH" \
    && true

SHELL ["/bin/bash", "-lc"]


# Copy only requirements, to cache them in docker layer:
WORKDIR /pysetup
COPY ./poetry.lock ./pyproject.toml /pysetup/
# Install basic requirements
RUN --mount=type=ssh pip3 install wheel virtualenv \
    && poetry export -f requirements.txt --without-hashes -o /tmp/requirements.txt \
    && pip3 wheel --wheel-dir=/tmp/wheelhouse --trusted-host 172.17.0.1 --find-links=http://172.17.0.1:3141 -r /tmp/requirements.txt \
    && virtualenv --system-site-packages /.venv && source /.venv/bin/activate && echo 'source /.venv/bin/activate' >>/root/.profile \
    && pip3 install --no-deps --trusted-host 172.17.0.1 --find-links=http://172.17.0.1:3141 --find-links=/tmp/wheelhouse/ /tmp/wheelhouse/*.whl \
    && true


# Timezone handling
ARG TIMEZONE="Europe/Helsinki"
RUN apk add --no-cache \
        tzdata \
    && cp /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
    && echo "$TIMEZONE" >/etc/timezone \
    && apk del tzdata \
    && true


#####################################
# Base stage for development builds #
#####################################
FROM builder_base as devel_build
# Install deps, docker is already isolated so we don't need virtualenv
WORKDIR /pysetup
RUN --mount=type=ssh poetry config virtualenvs.create false \
    && export PIP_FIND_LINKS=http://172.17.0.1:3141 \
    && export PIP_TRUSTED_HOST=172.17.0.1 \
    && export PIP_IGNORE_INSTALLED=1 \
    && poetry install -vv --no-interaction --no-ansi \
    && true


#############
# Run tests #
#############
FROM devel_build as test
COPY . /app
WORKDIR /app
# Re run install to get the service itself installed
RUN --mount=type=ssh poetry config virtualenvs.create false \
    && export PIP_FIND_LINKS=http://172.17.0.1:3141 \
    && export PIP_TRUSTED_HOST=172.17.0.1 \
    && export PIP_IGNORE_INSTALLED=1 \
    && poetry install -vv --no-interaction --no-ansi \
    && apk add --no-cache xvfb-run \
    && chmod a+x docker/entrypoint-test.sh \
    && docker/pre_commit_init.sh \
    && true
ENTRYPOINT ["/sbin/tini", "--", "docker/entrypoint-test.sh"]


###########
# Hacking #
###########
FROM test as devel_shell
RUN apk add --no-cache zsh \
    && sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" \
    && echo "if [ \"\$NO_WHEELHOUSE\" = \"1\" ]" >>/root/.profile \
    && echo "then" >>/root/.profile \
    && echo "  echo \"Wheelhouse disabled\"" >>/root/.profile \
    && echo "else">>/root/.profile \
    && echo "  export PIP_TRUSTED_HOST=172.17.0.1" >>/root/.profile \
    && echo "  export PIP_FIND_LINKS=http://172.17.0.1:3141" >>/root/.profile \
    && echo "fi" >>/root/.profile \
    && echo "source /root/.profile" >>/root/.zshrc \
    && pip3 install git-up \
    && true
ENTRYPOINT ["/bin/zsh", "-l"]
