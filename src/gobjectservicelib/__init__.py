"""GObject mainloop implementation of https://gitlab.com/advian-oss/python-datastreamservicelib"""
__version__ = "1.4.1"  # NOTE Use `bump2version --config-file patch` to bump versions correctly
