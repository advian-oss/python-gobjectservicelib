"""Test eventloop fetching"""
from typing import Optional

from gobjectservicelib import eventloop


def test_singleton() -> None:
    """Test the singleton fetcher"""
    loop1 = eventloop.singleton()
    assert eventloop.MAINLOOP_SINGLETON == loop1
    loop2 = eventloop.singleton()
    assert loop2 == loop1


def test_call_soon() -> None:
    """Test the call-soon helper"""
    change_value1: Optional[str] = None
    change_value2: Optional[str] = None
    change_value3: Optional[str] = None

    def callback1(arg1: str, arg2: str) -> None:
        """Set the change_value"""
        nonlocal change_value1, change_value2
        change_value1 = arg1
        change_value2 = arg2
        eventloop.singleton().quit()

    eventloop.call_soon(callback1, "foo", "bar")
    # TODO: check logs for the warning
    eventloop.singleton().run()
    assert change_value1 == "foo"
    assert change_value2 == "bar"

    def callback2() -> None:
        """Set the change_value"""
        nonlocal change_value3
        change_value3 = "baz"
        eventloop.singleton().quit()

    eventloop.call_soon(callback2)
    eventloop.singleton().run()
    assert change_value3 == "baz"


def test_call_soon_monkeypatch() -> None:
    """Test the monkeypatch for the helper"""
    loop = eventloop.singleton()
    change_value: Optional[str] = None

    def callback() -> None:
        """Set the change_value"""
        nonlocal change_value, loop
        change_value = "plom"
        loop.quit()

    loop.call_soon(callback)  # type: ignore
    loop.run()
    assert change_value == "plom"
