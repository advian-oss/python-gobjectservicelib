"""Test for the service baseclasses"""
import asyncio

import pytest

from .conftest import FlagService

# pylint: disable=W0621


@pytest.mark.asyncio
async def test_service_starts_and_quits(service_instance: FlagService) -> None:
    """Make sure the service starts and does not immediately die"""
    serv = service_instance
    # Put it as a task in the eventloop (instead of running it as blocking via run_until_complete)
    task = asyncio.get_event_loop().create_task(serv.run())
    # Wait a moment and make sure it's still up
    await asyncio.sleep(2)
    assert serv.setupflag
    assert not task.done()
    # Make sure we have default PUBlish socket
    assert serv.psmgr.default_pub_socket
    assert not serv.psmgr.default_pub_socket.closed
    # Make sure the config got loaded
    assert "zmq" in serv.config
    assert "pub_sockets" in serv.config["zmq"]
    assert serv._tasks["HEARTBEAT"]  # pylint: disable=W0212
    assert not serv._tasks["HEARTBEAT"].done()  # pylint: disable=W0212
    assert serv._gloop  # pylint: disable=W0212
    assert serv._gloop.is_running()  # type: ignore # pylint: disable=W0212

    # Tell the service to quit and check the task is done and exitcode is correct
    serv.quit()
    await asyncio.wait_for(task, timeout=0.5)
    FlagService.clear_exit_alarm()
    assert task.result() == 0
    assert serv.psmgr.default_pub_socket.closed
    assert "HEARTBEAT" not in serv._tasks  # pylint: disable=W0212
    assert not serv._gloop.is_running()  # pylint: disable=W0212
    assert serv.teardownflag


@pytest.mark.asyncio
async def test_service_fixture(running_service_instance: FlagService) -> None:
    """Test the running_service_instance fixture"""
    serv = running_service_instance
    assert serv._tasks["HEARTBEAT"]  # pylint: disable=W0212
    assert not serv._tasks["HEARTBEAT"].done()  # pylint: disable=W0212


@pytest.mark.asyncio
async def test_gloop_flag(running_service_instance: FlagService) -> None:
    """Test the gloop decorator"""
    serv = running_service_instance
    assert not serv.flag
    serv.set_flag_gloop(True)
    await asyncio.sleep(0.01)  # give the thread time
    assert serv.flag
    serv.set_flag_gloop(False)
    await asyncio.sleep(0.01)  # give the thread time
    assert not serv.flag
