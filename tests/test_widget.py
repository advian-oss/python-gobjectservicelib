"""Test the widget helper"""
from typing import cast
import asyncio
import threading
import logging

import pytest

from gobjectservicelib.widgets import ServiceWidgetBase, ServiceWindowBase
from gobjectservicelib.eventloop import aio, call_soon

from .conftest import FlagService


LOGGER = logging.getLogger(__name__)


class MyWidget(ServiceWidgetBase):
    """My cool widget"""

    @aio
    def set_service_flag(self, value: bool) -> None:
        """Set the flag in service by creating a task"""
        serv = cast(FlagService, self._service)

        async def inner(value: bool) -> bool:
            """Set the value and return it"""
            nonlocal serv
            cthread = threading.current_thread()
            mthread = threading.main_thread()
            LOGGER.debug("Setting the flag value, cthread={}, mthread={}".format(cthread, mthread))
            serv.flag = value
            return serv.flag

        cthread = threading.current_thread()
        mthread = threading.main_thread()
        LOGGER.debug("Creating task to set value, cthread={}, mthread={}".format(cthread, mthread))
        serv.create_task(inner(value))

    def set_flag_false(self) -> None:
        """proxy the set_service_flag"""
        cthread = threading.current_thread()
        mthread = threading.main_thread()
        LOGGER.debug("called, cthread={}, mthread={}".format(cthread, mthread))
        self.set_service_flag(False)

    def set_flag_true(self) -> None:
        """proxy the set_service_flag"""
        cthread = threading.current_thread()
        mthread = threading.main_thread()
        LOGGER.debug("called, cthread={}, mthread={}".format(cthread, mthread))
        self.set_service_flag(True)


class MyWindow(ServiceWindowBase):
    """My cool window"""

    def __init__(self, service: FlagService) -> None:
        """pass to super"""
        super().__init__(service, title="My cool window")


@pytest.mark.asyncio
async def test_window(running_service_instance: FlagService) -> None:
    """Test the window baseclass"""
    serv = running_service_instance
    win = MyWindow(serv)
    assert win._service  # pylint: disable=W0212
    assert win.get_title() == "My cool window"


@pytest.mark.asyncio
async def test_widget_aio_decorator(running_service_instance: FlagService) -> None:
    """test that the widget @aio decorator works"""
    serv = running_service_instance
    widget_instance = MyWidget(serv)

    async def wait_for_flag_val(val: bool) -> None:
        """wait for ser.flag to be given value"""
        nonlocal serv
        while serv.flag != val:
            await asyncio.sleep(0.1)

    # proxy func, no args
    call_soon(widget_instance.set_flag_true)
    await asyncio.wait_for(wait_for_flag_val(True), timeout=0.5)
    assert serv.flag
    call_soon(widget_instance.set_flag_false)
    await asyncio.wait_for(wait_for_flag_val(False), timeout=0.5)
    assert not serv.flag

    # the argumented func
    call_soon(widget_instance.set_service_flag, True)
    await asyncio.wait_for(wait_for_flag_val(True), timeout=0.5)
    assert serv.flag
    call_soon(widget_instance.set_service_flag, False)
    await asyncio.wait_for(wait_for_flag_val(False), timeout=0.5)
    assert not serv.flag
