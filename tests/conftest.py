"""auto fixtures etc"""
from typing import Dict, Any, AsyncGenerator
import asyncio
from pathlib import Path
from dataclasses import dataclass, field
import logging
import threading
import platform
import random

import tomlkit  # type: ignore
import pytest
from libadvian.logging import init_logging
from libadvian.testhelpers import nice_tmpdir  # pylint: disable=W0611
from datastreamservicelib.compat import asyncio_eventloop_check_policy, asyncio_eventloop_get


from gobjectservicelib.service import SimpleService
from gobjectservicelib.eventloop import gloop


# pylint: disable=W0621
init_logging(logging.DEBUG)
LOGGER = logging.getLogger(__name__)
asyncio_eventloop_check_policy()


@pytest.fixture
def event_loop():  # type: ignore
    """override pytest-asyncio default eventloop"""
    loop = asyncio_eventloop_get()
    LOGGER.debug("Yielding {}".format(loop))
    yield loop
    loop.close()


@dataclass
class FlagService(SimpleService):
    """Test service"""

    setupflag: bool = field(default=False)
    teardownflag: bool = field(default=False)
    flag: bool = field(default=False)

    async def post_gloop_setup(self) -> None:
        """set a flag"""
        await super().post_gloop_setup()
        self.setupflag = True

    async def pre_gloop_teardown(self) -> None:
        """set a flag"""
        await super().pre_gloop_teardown()
        self.teardownflag = True

    @gloop
    def set_flag_gloop(self, value: bool) -> None:
        """Set the flag value (in the gobject thread) actually you would do
        somthing with widgets etc"""
        cthread = threading.current_thread()
        mthread = threading.main_thread()
        LOGGER.debug("Setting the flag value, cthread={}, mthread={}".format(cthread, mthread))
        self.flag = value


@pytest.fixture
@pytest.mark.asyncio
async def service_instance(nice_tmpdir: str) -> FlagService:
    """Create a service instance for use with tests"""
    parsed: Dict[str, Any] = {"zmq": {"pub_sockets": []}}
    pub_sock_path = "ipc://" + str(Path(nice_tmpdir) / "flagservice_pub.sock")
    if platform.system() == "Windows":
        pub_sock_path = f"tcp://127.0.0.1:{random.randint(1337, 65000)}"  # nosec
    parsed["zmq"]["pub_sockets"] = [pub_sock_path]
    # Write a testing config file
    configpath = Path(nice_tmpdir) / "flagservice_testing.toml"
    with open(configpath, "wt", encoding="utf-8") as fpntr:
        fpntr.write(tomlkit.dumps(parsed))
    # Instantiate service and return it
    serv = FlagService(configpath)
    return serv


@pytest.fixture
@pytest.mark.asyncio
async def running_service_instance(service_instance: FlagService) -> AsyncGenerator[FlagService, None]:
    """Yield a running service instance, shut it down after the test"""
    task = asyncio.get_event_loop().create_task(service_instance.run())
    # Yield a moment so setup can do it's thing
    await asyncio.sleep(0.1)

    yield service_instance

    service_instance.quit()

    try:
        await asyncio.wait_for(task, timeout=2)
    except TimeoutError:
        task.cancel()
    finally:
        # Clear alarms and default exception handlers
        FlagService.clear_exit_alarm()
        asyncio.get_event_loop().set_exception_handler(None)
